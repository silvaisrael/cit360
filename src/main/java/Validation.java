import java.text.DecimalFormat;
import java.util.Scanner;

public class Validation {

    public static void main(String[] args) {

        int value1, value2;
        float result;
        Scanner scan = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("#.##");

        System.out.print("Enter first number: ");
        value1 = scan.nextInt();
        System.out.print("Enter second number: ");
        value2 = scan.nextInt();

        if (value2 == 0) {
            System.out.println("You cannot divide a number by 0.");
            System.out.print("Enter second number (not 0): ");
            value2 = scan.nextInt();
        }

        result = divide(value1, value2);

        System.out.println("The result of " + value1 + " divided by " + value2 + " is: " + df.format(result));

    }

    public static float divide(float num1, float num2) {

        return num1 / num2;
    }
}